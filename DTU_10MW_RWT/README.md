# Test case description

This test case consists of an 3D O-O mesh around the DTU 10MW Reference Wind Turbine.

## Grid

The grid is of O-O topology and has 128 x 64 x 64
cells in the chordwise, spanwise and normal
directions, respectively, with a tip cap of four blocks of 16x16 cells on each blade.
The domain radius is 10 rotor diameters.

## Case 1: w08turb

In this test case the flow is computed as fully turbulent at a wind speed of
8 m/s with the following overall input settings:

* Transient: False
* Schemes:
 * Momentum: quick quick uds
 * Pressure: simple
* Iterations: 100 100 100 (levels 1 2 3)
* Turbulence model: K-Omega SST
* Moving mesh

## Case 2: w08tran_drela

In this test case the flow is computed as transitional at a wind speed of
8 m/s with the following overall input settings:

* Transient: False
* Schemes:
 * Momentum: quick quick uds
 * Pressure: simple
* Iterations: 100 100 100 (levels 1 2 3)
* Turbulence model: K-Omega SST
* Transition model: Drela-Giles
* Moving mesh

## Case 1: w08turb_uns

In this test case the flow is computed as fully turbulent at a wind speed of
8 m/s with the following overall input settings:

* Transient: True
* Time steps: 0.004668591475 0.00933718295 0.00933718295
* Subiterations: 4 4
* Schemes:
 * Momentum: quick suds uds
 * Pressure: simple
* Iterations: 25 25 25 (levels 1 2 3)
* Turbulence model: K-Omega SST
* Moving mesh
