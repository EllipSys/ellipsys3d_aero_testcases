import os
import unittest
from ellipsyswrapper.testsuite import EllipSysTestCase

# check that the user has set the ELLIPSYS2D_PATH
if os.getenv("ELLIPSYS3D_PATH") == None:
    raise RuntimeError("ELLIPSYS3D_PATH environment variable not set!")

base_path = os.path.dirname(__file__)


class DTU_10MW_RWT_w08turb(unittest.TestCase, EllipSysTestCase):
    def setUp(self):
        self.casedict = {
            "solver": "ellipsys3d",
            "directory": os.path.join(
                base_path, "w08turb"
            ),
            "inputfile": "input.dat",
            "inputs": [{"mstep": [50, 100, 100]}, {"project": "grid"}],
            "variables": ["fz_ave", "mz_ave"],
            "test_restart": True,
            "debug": True,
            "nstep_ave": 20,
            "nprocs": 8,
        }


class DTU_10MW_RWT_w08tran_drela(unittest.TestCase, EllipSysTestCase):
    def setUp(self):
        self.casedict = {
            "solver": "ellipsys3d",
            "directory": os.path.join(
                base_path, "w08tran_drela"
            ),
            "inputfile": "input.dat",
            "inputs": [{"mstep": [50, 100, 100]}, {"project": "grid"}],
            "variables": ["fz_ave", "mz_ave"],
            "debug": True,
            "nstep_ave": 20,
            "nprocs": 8,
        }


class DTU_10MW_RWT_w08turb_uns(unittest.TestCase, EllipSysTestCase):
    def setUp(self):
        self.casedict = {
            "solver": "ellipsys3d",
            "directory": os.path.join(
                base_path, "w08turb_uns"
            ),
            "inputfile": "input.dat",
            "inputs": [{"mstep": [25, 25, 25]}, {"project": "grid"}],
            "variables": ["fz_ave", "mz_ave"],
            "test_restart": True,
            "debug": True,
            "nstep_ave": 20,
            "nprocs": 8,
        }


if __name__ == "__main__":
    unittest.main()
