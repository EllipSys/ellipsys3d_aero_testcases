import os
import unittest
from ellipsyswrapper.testsuite import EllipSysTestCase

# check that the user has set the ELLIPSYS2D_PATH
if os.getenv("ELLIPSYS3D_PATH") == None:
    raise RuntimeError("ELLIPSYS3D_PATH environment variable not set!")

base_path = os.path.dirname(__file__) 


class FFA_W3_241_pseudo3d_laminar(unittest.TestCase, EllipSysTestCase):
    def setUp(self):
        self.casedict = {
            "solver": "ellipsys3d",
            "directory": os.path.join(
                base_path,
                "re10e3laminar",
            ),
            "inputfile": "input.dat",
            "inputs": [
                {"mstep": [100, 100, 100]},
                {"uinlet": 1.0},
                {"vinlet": 0.0},
                {"ufarfield": 1.0},
                {"vfarfield": 0.0},
                {"project": "grid"},
            ],
            "test_restart": True,
            "debug": True,
            "variables": ["fx_ave", "fy_ave"],
            "nprocs": 4,
        }


class FFA_W3_241_pseudo3d_turb(unittest.TestCase, EllipSysTestCase):
    def setUp(self):
        self.casedict = {
            "solver": "ellipsys3d",
            "directory": os.path.join(
                base_path,
                "re10e6turb",
            ),
            "inputfile": "input.dat",
            "inputs": [
                {"mstep": [100, 100, 100]},
                {"uinlet": 1.0},
                {"vinlet": 0.0},
                {"ufarfield": 1.0},
                {"vfarfield": 0.0},
                {"project": "grid"},
            ],
            "test_restart": True,
            "debug": True,
            "nstep_ave": 20,
            "variables": ["fx_ave", "fy_ave"],
            "nprocs": 4,
        }


class FFA_W3_241_pseudo3d_turb_rough(unittest.TestCase, EllipSysTestCase):
    def setUp(self):
        self.casedict = {
            "solver": "ellipsys3d",
            "directory": os.path.join(
                base_path,
                "re10e6turb-rough",
            ),
            "inputfile": "input.dat",
            "inputs": [
                {"mstep": [100, 100, 100]},
                {"uinlet": 1.0},
                {"vinlet": 0.0},
                {"ufarfield": 1.0},
                {"vfarfield": 0.0},
                {"project": "grid"},
            ],
            "test_restart": True,
            "debug": True,
            "nstep_ave": 20,
            "variables": ["fx_ave", "fy_ave"],
            "nprocs": 4,
        }


class FFA_W3_241_pseudo3d_turb_rough_trip(unittest.TestCase, EllipSysTestCase):
    def setUp(self):
        self.casedict = {
            "solver": "ellipsys3d",
            "directory": os.path.join(
                base_path,
                "re10e6turb-rough-trip",
            ),
            "inputfile": "input.dat",
            "inputs": [
                {"mstep": [100, 100, 100]},
                {"uinlet": 1.0},
                {"vinlet": 0.0},
                {"ufarfield": 1.0},
                {"vfarfield": 0.0},
                {"project": "grid"},
            ],
            "test_restart": True,
            "debug": True,
            "nstep_ave": 20,
            "variables": ["fx_ave", "fy_ave"],
            "nprocs": 4,
        }


if __name__ == "__main__":
    unittest.main()
