# Test case description

This test case consists of an extruded 2D airfoil mesh on the FFA-W3-241 airfoil.

## Grid

The grid is an extruded O-grid and has 128 x 64 x 32
cells in the chordwise, normal and spanwise
directions, respectively, with a domain radius of 20c. Symmetry conditions are applied on the side boundaries.

## Case 1: re10e3laminar

In this test case the flow is assumed laminar at a Reynolds number of
10e3 with the following overall input settings:

* Transient: False
* Schemes:
 * Momentum: quick quick quick
 * Pressure: simple
* Iterations: 100 100 100 (levels 1 2 3)
* Inflow angle: 0 deg.
* Turbulence model: None

## Case 2: re10e6turb

In this test case the flow is computed as fully turbulent at a Reynolds number of
10e6 with the following overall input settings:

* Transient: False
* Schemes:
 * Momentum: quick quick quick
 * Pressure: simple
* Iterations: 100 100 100 (levels 1 2 3)
* Inflow angle: 0 deg.
* Turbulence model: K-Omega SST
